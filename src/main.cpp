#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET 4        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Conexión WiFi
const char *ssid = "*********";
const char *password = "**********";

// Cliente ntpUDP
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

// Parámetros de conexión de OpenWheaterMap
String apiKey = "*****************";
String ciudad = "Río%20Gallegos";
String pais = "AR";
String urlServidor = "http://api.openweathermap.org/data/2.5/weather?q=" + ciudad + "," + pais + "&APPID=" + apiKey + "&units=metric";

void setup()
{
  Serial.begin(115200);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(2000); // Pause for 2 seconds

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  display.clearDisplay();

  display.setTextSize(1);              // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);             // Start at top-left corner
  display.println(WiFi.localIP());

  display.display();

  delay(2000);

  // Ajusto el cliente UDP
  timeClient.begin();
  timeClient.setTimeOffset(-10800);
}

void loop()
{
  HTTPClient http;
  String response = "{}";

  http.begin(urlServidor);
  int httpResponseCode = http.GET();

  if (httpResponseCode > 0)
  {
    response = http.getString();
  }

  // No necesito más el cliente
  http.end();

  timeClient.update();
  String fecha = timeClient.getFormattedTime();

  DynamicJsonDocument jsonData(1024);
  DeserializationError error = deserializeJson(jsonData, response);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.println(ciudad + " " + pais);
  display.println(fecha);

  if (!error)
  {
    int temperatura = jsonData["main"]["temp"];
    int presion = jsonData["main"]["pressure"];
    int humedad = jsonData["main"]["humidity"];

    display.println("Temper.: " + String(temperatura) + " C");
    display.println("Presion: " + String(presion) + " hPa");
    display.println("Humedad: " + String(humedad) + " %");
  }
  else
  {
    display.println(error.f_str());
  }

  display.display();

  delay(10000);
}
